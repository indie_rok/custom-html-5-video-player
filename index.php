<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Audio Player</title>
	<meta name="viewport" content="width=device-width, user-scalable=yes">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<script type="text/javascript" src="js/jQuery.js"></script>
	<script type="text/javascript" src="js/screenfull.js"></script>
    <?php $tracks = simplexml_load_file('screencast.xml'); ?>
</head>

<body>

	<article id="container">
		
		<h1><?php echo $tracks->audio[0]?></h1>

		<video src="<?php echo $tracks->video[0]; ?>" type="<?php echo $tracks->type[0]; ?>" id="videoPlayer"></video>

		<div id="controlsCointainer">
			<button id="playPauseBut" class="play"></button>

			<div id="dragableContainer">
				<div id="dragableBar">
					<div id="dragableBlock" draggable="true"></div>
				</div>
			</div>
			<div id="timer">
				<div id="currentSecond">0:00</div>
				<span>/</span>
				<div id="totalVideoDuration"></div>
			</div>
			
			<div id="volumeContainer2">
				<div id="vol0" class="volumeBar"></div>
				<div id="vol1" class="volumeBar"></div>
				<div id="vol2" class="volumeBar"></div>
				<div id="vol3" class="volumeBar"></div>
				<div id="vol4" class="volumeBar"></div>
				<div id="vol5" class="volumeBar"></div>
				<div id="vol6" class="volumeBar"></div>
				<div id="vol7" class="volumeBar"></div>
				<div id="vol8" class="volumeBar"></div>
				<div id="vol9" class="volumeBar"></div>
				<div id="vol10" class="volumeBar"></div>
				<div id="vol11" class="volumeBar"></div>
			</div>

			<button id="fullScreen" class="fullscreen"></button>
		</div>
		
	</article>

<script type="text/javascript" src="js/functions.js"></script>	
</body>
</html>