	var videoPlayer = $("#videoPlayer").get(0);
	var playPauseBut = $("#playPauseBut");
	var progressBar = $("#dragableBar");
	var totalVideoDuration = $("#totalVideoDuration");
	var currentSecond = $("#currentSecond");
	var fullScreen = $("#fullScreen");
	var timerBlock = $("#dragableBlock");
	var progressWidth = $("#dragableBar").width();


$( window ).load(function() {

	videoPlayer.volume=0.5;

	playPauseBut.on( "click", playPause);

	$("#videoPlayer").on("loadedmetadata",updateTotalVideoTimmer);
	$("#videoPlayer").on("timeupdate",updateCurrentSecond);
	$("#videoPlayer").on("timeupdate",updateProgressBar);
	$("#videoPlayer").on("click",playPause);

	$("#dragableContainer").on("click",function(event){
		
		if(videoPlayer.paused){
			playPause();
		}

		totalProgressWithInPx = event.pageX - $("#dragableBar").offset().left;
		videoPorcentaje = ( totalProgressWithInPx * 100) / progressWidth;

		if(videoPorcentaje>100){
			videoPorcentaje = 100;
		}
		secondToPlay = (videoPorcentaje * videoPlayer.duration) / 100;
		videoPlayer.currentTime = secondToPlay;
		videoPlayer.play();
	});

	$("#volumeContainer2").on("click",function(){
		volumeLevelInPx = event.pageX - $("#volumeContainer2").offset().left;
		volumeWidth = $("#volumeContainer2").width();
		percentajeOfVolume = volumeLevelInPx / volumeWidth;
		percentajeOfVolume = Math.floor(percentajeOfVolume*100);

		//define the parameters of the bars
		volumeValues=[
			[0,0],
			[1,7],
			[8,15],
			[16,23],
			[24,31],
			[32,39],
			[40,47],
			[48,55],
			[56,63],
			[64,71],
			[72,79],
			[80,85],
			[86,90],
			[91,100]
		];

		if(percentajeOfVolume<0){
			percentajeOfVolume=0;
		}

		//reset all the bars to a1a1a1
		$(".volumeBar").css("background-color","#a1a1a1")


		//do the changes depending on the click source
		for(i=0;i<volumeValues.length;i++){
			if(volumeValues[i][0]<= percentajeOfVolume && volumeValues[i][1] >= percentajeOfVolume){
				for(w=0;w<i;w++){
					console.log(i);
					console.log("--");
					$("#vol"+w+"").css("background","#000");
				}
					break;
			}
		}

		videoPlayer.volume = percentajeOfVolume/100;

	});

	fullScreen.on("click",function(){
		screenfull.request($("#container").get(0));
	});
})


function playPause(){
	if(videoPlayer.paused){
		playPauseBut.attr("class","pause")
		videoPlayer.play();
	}
	else{
		playPauseBut.attr("class","play")
		videoPlayer.pause();
	}
}

function updateCurrentSecond(){
	if(videoPlayer.currentTime == 0){
		currentSecond.text("0:00");
	}else{
		formatedTime = getFormatedTime(videoPlayer.currentTime);
		currentSecond.text(formatedTime);
	}
}

function updateTotalVideoTimmer(){
	formatedTime = getFormatedTime(videoPlayer.duration);
	totalVideoDuration.text(formatedTime);
}

function getFormatedTime(time){
	
	var formatedTime = time / 100;
	formatedTime = formatedTime.toString();
	formatedTime = formatedTime.substr(0,4);
	formatedTime = formatedTime.replace(".",":");
	return formatedTime;
}

function updateProgressBar(){
	var currentTime = videoPlayer.currentTime;
	var totalVideoTime = videoPlayer.duration;
	var progress = (currentTime * progressWidth )/ totalVideoTime ;
	$("#dragableBlock").css("left",progress);
}

function changeVideoStartPoint(event){
	
}